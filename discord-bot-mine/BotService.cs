using System;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;

namespace discord_bot_mine;

public class BotService
{
    ConfigurationService m_configurationService;
    ShitpostHandlerService m_shitpostHandlerService;
    DiscordSocketClient m_discord;
    CancellationToken m_cancellationToken;

    public BotService()
    {
        m_configurationService = new ConfigurationService();
        m_shitpostHandlerService = new ShitpostHandlerService();
        m_discord = new DiscordSocketClient();
        m_discord.Connected += HandleConnect;
        m_discord.Disconnected += HandleDisconnect;
        m_discord.MessageReceived += HandleMessageReceive;
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        m_cancellationToken = cancellationToken;
        Console.WriteLine("Token: [{0}]", m_configurationService.Token);

        while (!m_cancellationToken.IsCancellationRequested)
        {
            try
            {
                Console.WriteLine("Logging in to discord");
                await m_discord.LoginAsync(
                    TokenType.Bot,
                    m_configurationService.Token,
                    true);
                
                Console.WriteLine("Starting discord connection");
                await m_discord.StartAsync();

                Console.WriteLine("Setting bot status");
                await m_discord.SetStatusAsync(UserStatus.Online);
                await m_discord.SetActivityAsync(new BotActivity());

                Console.WriteLine("Bot is ready");
                break;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error starting bot:");
                Console.WriteLine(ex.ToString());
                await Task.Delay(TimeSpan.FromSeconds(5));
            }
        }

        while (!m_cancellationToken.IsCancellationRequested)
        {
            await Task.Delay(TimeSpan.FromSeconds(1));
        }

        m_discord.Dispose();
        Console.WriteLine("Bot stopped");
    }

    private Task HandleConnect()
    {
        Console.WriteLine("Connected to discord");
        return Task.CompletedTask;
    }

    private Task HandleDisconnect(Exception ex)
    {
        Console.WriteLine("Disconnected from discord");
        Console.WriteLine(ex.ToString());
        return Task.CompletedTask;
    }

    private async Task HandleMessageReceive(SocketMessage arg)
    {
        SocketUserMessage? msg = arg as SocketUserMessage;

        if (msg == null)
        {
            return;
        }

        try
        {
            await m_shitpostHandlerService.HandleAsync(
                msg,
                m_cancellationToken);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Message received handler error:");
            Console.WriteLine(ex.ToString());
        }
    }
}
