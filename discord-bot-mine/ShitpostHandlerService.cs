using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;

namespace discord_bot_mine;

public class ShitpostHandlerService
{
    ulong[] CH_IDS = new[]
    {
        932871740445564958U,
        839827748783915028U
    };

    ulong[] USER_IDS = new[]
    {
        392944493420216324U
    };

    async Task DeleteAsync(IUserMessage msg, CancellationToken cancellationToken)
    {
        try
        {
            await Task.Delay(
                TimeSpan.FromMinutes(1.5),
                cancellationToken);
            
            RequestOptions req = new RequestOptions()
            {
                CancelToken = cancellationToken
            };
            await msg.DeleteAsync(req);
            Console.WriteLine(
                "Shitpost deleted from '{0}' by '{1}'",
                msg.Channel.Name,
                msg.Author.Username);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Unable to delete post:");
            Console.WriteLine(ex.ToString());
        }
    }

    public Task HandleAsync(IUserMessage msg, CancellationToken cancellationToken)
    {
        if (!CH_IDS.Contains(msg.Channel.Id))
        {
            return Task.CompletedTask;
        }

        if (!USER_IDS.Contains(msg.Author.Id))
        {
            return Task.CompletedTask;
        }

        Console.WriteLine(
            "Shitposter detected in '{0}' by '{1}'",
            msg.Channel.Name,
            msg.Author.Username);
        _ = DeleteAsync(msg, cancellationToken);
        return Task.CompletedTask;
    }
}
