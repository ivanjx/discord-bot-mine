using System;

namespace discord_bot_mine;

public class ConfigurationService
{
    public string Token
    {
        get;
        private set;
    }
    
    public ConfigurationService()
    {
        string? token = Environment.GetEnvironmentVariable("TOKEN");

        if (string.IsNullOrEmpty(token))
        {
            throw new Exception("Token is empty");
        }

        Token = token;
    }
}
