﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace discord_bot_mine;

public class Program
{
    static CancellationTokenSource m_cts;
    static Task m_botTask;

    static Program()
    {
        m_cts = new CancellationTokenSource();
        m_botTask = Task.CompletedTask;
    }

    public static void Main()
    {
        Console.WriteLine("Starting bot");
        AppDomain.CurrentDomain.ProcessExit += HandleSigterm;
        Console.CancelKeyPress += HandleSigint;

        BotService bot = new BotService();
        m_botTask = bot.StartAsync(m_cts.Token);
        m_botTask.Wait();
    }

    static void HandleSigint(object? sender, ConsoleCancelEventArgs e)
    {
        e.Cancel = true;
        Console.WriteLine("SIGINT received");
        m_cts.Cancel();
        m_botTask.Wait();
    }

    static void HandleSigterm(object? sender, EventArgs e)
    {
        Console.WriteLine("SIGTERM received");
        m_cts.Cancel();
        m_botTask.Wait();
    }
}