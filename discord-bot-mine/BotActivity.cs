using System;
using Discord;

namespace discord_bot_mine;

public class BotActivity : IActivity
{
    public string Name => "君が代";

    public ActivityType Type => ActivityType.Playing;

    public ActivityProperties Flags => ActivityProperties.Instance;

    public string Details => "Playing " + Name;
}
