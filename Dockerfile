# dotnet sdk.
FROM mcr.microsoft.com/dotnet/sdk:6.0.302-focal as build
COPY ./discord-bot-mine /discord-bot-mine
WORKDIR /discord-bot-mine
RUN dotnet publish \
    -c Release \
    -r linux-arm64 \
    -p:PublishSingleFile=true \
    -p:PublishReadyToRun=true \
    --self-contained false \
    -o output

# dotnet runtime.
FROM mcr.microsoft.com/dotnet/runtime:6.0.7-focal-arm64v8 as runtime
COPY --from=build /discord-bot-mine/output /app

# Entry point.
ENTRYPOINT [ "/app/discord-bot-mine" ]
